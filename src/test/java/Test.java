import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
//@ExtendWith(CustomTestExecutionListener.class)
public class Test {

    @ParameterizedTest(name = "{index} => number={0}, isEven={1}")
    @CsvFileSource(resources = "/1.csv", numLinesToSkip = 1)
    @DisplayName("Test isEven method with different parameters")
    void testIsEven(int number, boolean expected) {
        assertEquals(expected, isEven(number));
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}